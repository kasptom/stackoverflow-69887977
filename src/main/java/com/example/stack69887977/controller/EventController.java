package com.example.stack69887977.controller;

import com.example.stack69887977.model.Event;
import com.example.stack69887977.repository.EventRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.List;

@RestController
public class EventController {
    private final EventRepository eventRepository;

    public EventController(EventRepository eventRepository) {
        this.eventRepository = eventRepository;
    }

    @GetMapping("events")
    public ResponseEntity<List<Event>> getEvents(@RequestParam(value = "limit", defaultValue = "10") Integer limit) {
        return ResponseEntity.ok(eventRepository.getEvents(
                LocalDateTime.parse("2021-12-03T10:15:30"),
                LocalDateTime.parse("2021-12-03T10:15:30"),
                "UTC",
                limit
        ));
    }

    @GetMapping("ez-events")
    public ResponseEntity<List<Event>> getEzEvents() {
        return ResponseEntity.ok(eventRepository.findAll());
    }
}
