package com.example.stack69887977.model;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "location")
public class Location extends BaseEntity {
    public String name;

    public Location(String id) {
        this.id = id;
    }

    public Location() {
    }
}
