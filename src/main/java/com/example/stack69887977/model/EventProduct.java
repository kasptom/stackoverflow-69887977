package com.example.stack69887977.model;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class EventProduct extends BaseEntity {
    public String name;

    @Column(name = "product_id")
    public String product;
}
