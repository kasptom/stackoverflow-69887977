INSERT INTO location(id, name) values ('fe88fa63-360e-4281-a383-35826a759e59', 'room 42');
INSERT INTO location(id, name) values ('e7193453-4c1b-433d-8127-5eedf2efd011', 'room 43');

INSERT INTO events(id, starts_at, ends_at, frequency, separation, timezone_name, is_full_day, location_id) VALUES ('d255afb6-3730-4845-8e5e-18baa909dcfe', '2021-12-21 18:00:00.000', '2021-12-22 02:00:00.000', 'ONCE', 1, 'UTC', FALSE, 'fe88fa63-360e-4281-a383-35826a759e59');
INSERT INTO events(id, starts_at, ends_at, frequency, separation, timezone_name, is_full_day, location_id) VALUES ('e7193453-4c1b-433d-8127-5eedf2efd011', '2022-02-20 20:00:00.000', '2022-02-20 23:00:00.000', 'WEEKLY', 1, 'UTC', FALSE, 'fe88fa63-360e-4281-a383-35826a759e59');

