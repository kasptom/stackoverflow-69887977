INSERT INTO recurring_events_mock
(id, starts_at, ends_at, frequency, separation, count, until, timezone_name, is_full_day, location_id, created_at,
 updated_at, version, name, description, picture)
VALUES ('d255afb6-3730-4845-8e5e-18baa909dcfe', '2021-12-21 18:00:00.000', '2021-12-22 02:00:00.000', 'ONCE', 1, null,
        null,
        'UTC', false, 'fe88fa63-360e-4281-a383-35826a759e59', '2021-10-29 00:22:46.109', '2021-10-29 00:22:46.109', 0,
        '', '', '');
INSERT INTO recurring_events_mock
(id, starts_at, ends_at, frequency, separation, count, until, timezone_name, is_full_day, location_id, created_at,
 updated_at, version, name, description, picture)
VALUES ('e7193453-4c1b-433d-8127-5eedf2efd011', '2022-02-20 20:00:00.000', '2022-02-20 23:00:00.000', 'WEEKLY', 1, null,
        null,
        'UTC', false, 'fe88fa63-360e-4281-a383-35826a759e59', '2021-11-02 19:23:31.029', '2021-11-02 19:23:31.029', 0,
        'Festa da minha graaande amiga', 'A Fofinha nao vai', '');
INSERT INTO recurring_events_mock
(id, starts_at, ends_at, frequency, separation, count, until, timezone_name, is_full_day, location_id, created_at,
 updated_at, version, name, description, picture)
VALUES ('e7193453-4c1b-433d-8127-5eedf2efd011', '2022-02-21 20:00:00.000', '2022-02-21 23:00:00.000', 'WEEKLY', 1, null,
        null,
        'UTC', false, 'fe88fa63-360e-4281-a383-35826a759e59', '2021-11-02 19:23:31.029', '2021-11-02 19:23:31.029', 0,
        'Muito massa essa fita        ', 'Mudanca drastica', '');
INSERT INTO recurring_events_mock
(id, starts_at, ends_at, frequency, separation, count, until, timezone_name, is_full_day, location_id, created_at,
 updated_at, version, name, description, picture)
VALUES ('e7193453-4c1b-433d-8127-5eedf2efd011', '2022-02-22 20:00:00.000', '2022-02-22 23:00:00.000', 'WEEKLY', 1, null,
        null,
        'UTC', false, 'fe88fa63-360e-4281-a383-35826a759e59', '2021-11-02 19:23:31.029', '2021-11-02 19:23:31.029', 0,
        'Festa da minha graaande amiga', 'A Fofinha nao vai', '');
INSERT INTO recurring_events_mock
(id, starts_at, ends_at, frequency, separation, count, until, timezone_name, is_full_day, location_id, created_at,
 updated_at, version, name, description, picture)
VALUES ('e7193453-4c1b-433d-8127-5eedf2efd011', '2022-02-24 20:00:00.000', '2022-02-24 23:00:00.000', 'WEEKLY', 1, null,
        null,
        'UTC', false, 'fe88fa63-360e-4281-a383-35826a759e59', '2021-11-02 19:23:31.029', '2021-11-02 19:23:31.029', 0,
        'Festa da minha graaande amiga', 'A Fofinha nao vai', '');
INSERT INTO recurring_events_mock
(id, starts_at, ends_at, frequency, separation, count, until, timezone_name, is_full_day, location_id, created_at,
 updated_at, version, name, description, picture)
VALUES ('e7193453-4c1b-433d-8127-5eedf2efd011', '2022-02-25 20:00:00.000', '2022-02-25 23:00:00.000', 'WEEKLY', 1, null,
        null,
        'UTC', false, 'fe88fa63-360e-4281-a383-35826a759e59', '2021-11-02 19:23:31.029', '2021-11-02 19:23:31.029', 0,
        'Festa da minha graaande amiga', 'A Fofinha nao vai', '');
