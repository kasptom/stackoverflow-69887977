package com.example.stack69887977.model;

import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public class BaseEntity extends BaseEntityNoId {
    @Id public String id;
}
