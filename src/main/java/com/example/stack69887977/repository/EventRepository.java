package com.example.stack69887977.repository;

import com.example.stack69887977.model.Event;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface EventRepository extends CrudRepository<Event, String> {
    @Query(name = "Event.RecurringEventsQuery")
    List<Event> getEvents(
            @Param("range_start") LocalDateTime rangeStart,
            @Param("range_end") LocalDateTime rangeEnd,
            @Param("time_zone") String timezone,
            @Param("events_limit") Integer limit
    );
    List<Event> findAll();
}
