CREATE TABLE recurring_events_mock(
    id VARCHAR(50),
    starts_at DATETIME,
    ends_at DATETIME,
    frequency VARCHAR(50),
    separation INT,
    count INT,
    until DATETIME,
    timezone_name VARCHAR(30),
    is_full_day BOOLEAN,
    location_id VARCHAR(50),
    created_at DATETIME,
    updated_at DATETIME,
    version INT,
    name VARCHAR(50),
    description VARCHAR(50),
    picture VARCHAR(50)
);
