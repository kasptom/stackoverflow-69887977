package com.example.stack69887977.model;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Set;

@Entity
@Table(name = "events")
@SqlResultSetMapping(
        name = "recurring_events_mapping",
        classes = {
                @ConstructorResult(
                        targetClass = Event.class,
                        columns = {
                                @ColumnResult(name = "ID", type = String.class),
                                @ColumnResult(name = "STARTS_AT", type = LocalDateTime.class),
                                @ColumnResult(name = "ENDS_AT", type = LocalDateTime.class),
                                @ColumnResult(name = "FREQUENCY", type = String.class),
                                @ColumnResult(name = "SEPARATION", type = Integer.class),
                                @ColumnResult(name = "COUNT", type = Integer.class),
                                @ColumnResult(name = "UNTIL", type = LocalDate.class),
                                @ColumnResult(name = "TIMEZONE_NAME", type = String.class),
                                @ColumnResult(name = "IS_FULL_DAY", type = Boolean.class),
                                @ColumnResult(name = "LOCATION_ID", type = String.class),
                                @ColumnResult(name = "CREATED_AT", type = LocalDateTime.class),
                                @ColumnResult(name = "UPDATED_AT", type = LocalDateTime.class),
                                @ColumnResult(name = "VERSION", type = Integer.class),
                                // metadata
                                @ColumnResult(name = "NAME", type = String.class),
                                @ColumnResult(name = "DESCRIPTION", type = String.class),
                                @ColumnResult(name = "PICTURE", type = String.class)
                        }
                )
        }
)
@NamedNativeQuery(
        name = "Event.RecurringEventsQuery",
        query = "select  e.*, " +
                "    coalesce (em.name, em2.name) as name, " +
                "    coalesce (em.description, em2.description) as description, " +
                "    coalesce (em.picture , em2.picture) as picture " +
                "from recurring_events_mock e " + // just for test
                "left join events_metadata em on " +
                "    em.event_id = e.id " +
                "    and em.starts_at = e.starts_at " +
                "left join events_metadata em2 on " +
                "    em2.event_id = e.id " +
                "    and em2.starts_at = ( " +
                "    select MIN(starts_at) " +
                "    from events_metadata " +
                "    where event_id = e.id " +
                "    limit 1)" +
                "where :time_zone IS NOT NULL " + // just for test
                "    and :events_limit > 0 " + // just for test
                "    and :range_start IS NOT NULL " + // just for test
                "    and :range_end IS NOT NULL ", // just for test
        resultSetMapping = "recurring_events_mapping")
public class Event extends BaseEntity {
    @Column
    public LocalDateTime startsAt;

    @Column
    public LocalDateTime endsAt;

    @Column
    @Enumerated(EnumType.STRING)
    private EventFrequency frequency;

    @Column(nullable = false)
    private Integer separation;

    @Column
    private Integer count;

    @Column
    private LocalDate until;

    @Column(nullable = false)
    private String timezoneName;

    @Column(nullable = false)
    private boolean isFullDay;

    @ManyToMany
    @JoinTable(
            name = "event_organizers",
            joinColumns = @JoinColumn(name = "event_id"),
            inverseJoinColumns = @JoinColumn(name = "user_id"))
    private Set<User> organizers;

    @OneToMany(mappedBy = "product")
    private Set<EventProduct> products;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = "location_id")
    private Location location;

    @OneToMany(mappedBy = "event",
            orphanRemoval = true,
            cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    private Set<EventRecurrence> recurrences;

    @OneToOne(mappedBy = "id.event", optional = false, orphanRemoval = true, fetch = FetchType.LAZY)
    public EventMetadata metadata;

    public Event(String id) {
        this.id = id;
    }

    public Event() {
    }

    public Event(
            String id,
            LocalDateTime startsAt,
            LocalDateTime endsAt,
            String frequency,
            Integer separation,
            Integer count,
            LocalDate until,
            String timezoneName,
            Boolean isFullDay,
            String locationId,
            LocalDateTime createdAt,
            LocalDateTime updatedAt,
            Integer version,
            // metadata
            String name,
            String description,
            String picture) {
        this.id = id;
        this.startsAt = startsAt;
        this.endsAt = endsAt;
        this.frequency = EventFrequency.valueOf(frequency);
        this.separation = separation;
        this.count = count;
        this.until = until;
        this.timezoneName = timezoneName;
        this.isFullDay = isFullDay;
        this.location = new Location(locationId);
        this.metadata = new EventMetadata(id, startsAt, name, description, picture);
    }
}