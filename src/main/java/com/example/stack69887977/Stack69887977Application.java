package com.example.stack69887977;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Stack69887977Application {

    // https://stackoverflow.com/questions/69887977/custom-join-with-composite-key-hibernate
    public static void main(String[] args) {
        SpringApplication.run(Stack69887977Application.class, args);
    }

}
