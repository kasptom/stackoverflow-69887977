package com.example.stack69887977.model;

public enum EventFrequency {
    ONCE, WEEKLY
}
