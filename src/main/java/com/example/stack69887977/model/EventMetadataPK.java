package com.example.stack69887977.model;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Embeddable
public class EventMetadataPK implements Serializable {

    @OneToOne(orphanRemoval = true)
    @JoinColumn(name = "event_id", updatable = false, insertable = false)
    private Event event;

    @Column(nullable = false)
    private LocalDateTime startsAt;

    public EventMetadataPK(Event event, LocalDateTime startsAt) {
        this.event = event;
        this.startsAt = startsAt;
    }

    public EventMetadataPK() {
    }
}