package com.example.stack69887977.model;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Set;

@Table(name = "events_metadata")
@Entity
public class EventMetadata extends BaseEntityNoId {

    @EmbeddedId
    private EventMetadataPK id;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    public String description;

    @Column
    private String picture;

    @Column(nullable = false)
    private boolean isPrivate;

    @Column(nullable = false)
    private boolean showGuestList;

    @Column(nullable = false)
    private boolean friendCanInviteFriends;

    @OneToMany(mappedBy = "event", orphanRemoval = true)
    private Set<EventAttendance> eventAttendances;

    public EventMetadata(String eventId, LocalDateTime startsAt, String name, String description, String picture) {
        this.id = new EventMetadataPK(new Event(eventId), startsAt);
        this.name = name;
        this.description = description;
        this.picture = picture;
    }

    public EventMetadata() {

    }
}